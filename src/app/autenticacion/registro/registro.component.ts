import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  registroForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router:Router
  ) {}

  ngOnInit(): void {
    this.forms();
  }

  forms(): void {
    this.registroForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      password: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
    });
  }

  registrar() {
    

    if (this.registroForm.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        html: `${this.mensajeValidacion(this.registroForm.value)}`,
      });

      return;
    }
    this.authService.registrarUsuario(this.registroForm.value).subscribe(
      (resp) => {
        Swal.fire({
          icon: 'success',
          title: 'Good job!',
          text: `Registro exitoso`,
        });

        setTimeout(() => {
          this.router.navigate([''])
        }, 2000);

      },
      (err) => {
        
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: `${err.error.errors.email[0]}`,
        });
      }
    );
  }

  mensajeValidacion(data: any) {
    let li = '';

    if (data.name == null) {
      li += `<li>El nombre es requerido</li>`;
    }
    if (data.password == null) {
      li += `<li>La contraseña es requerida</li>`;
    }
    if (data.email == null) {
      li += `<li>El correo electronico es requerido</li>`;
    }

    let message = `
      <ul>
        ${li}
      </ul>

    `;

    return message;
  }
}
