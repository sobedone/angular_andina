import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  uri_api:string = 'http://127.0.0.1:8000/api'
  token:string =""
  constructor(private http:HttpClient) {
    this.token = localStorage.getItem('token') || ''
  
  }


  registrarUsuario(form:any):Observable<any>{
    const url =`${this.uri_api}/usuarios`
    return this.http.post<any>(url,form)
  }

  loginUsuario(form:any):Observable<any>{
    const url =`${this.uri_api}/usuarios/token`
    return this.http.post<any>(url,form)
  }

  logoutUsuario():Observable<any>{
    this.token = localStorage.getItem('token') || ''
    const headers = new HttpHeaders({
      'Authorization':'Bearer '+this.token
    })
    console.log(headers,this.token)
    const url =`${this.uri_api}/logout`
    return this.http.get<any>(url,{headers})
  }
  


}
