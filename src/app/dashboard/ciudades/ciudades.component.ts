import { Component, OnInit } from '@angular/core';
import { CiudadesService } from '../services/ciudades.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-ciudades',
  templateUrl: './ciudades.component.html',
  styleUrls: ['./ciudades.component.css']
})
export class CiudadesComponent implements OnInit {
  rutaBtn:string ='/dashboard/ciudades/crear'
  ocultarTabla:boolean = false
  ciudades:any[] =[]
  constructor(private ciudadesService:CiudadesService) { }
  
  ngOnInit(): void {
    this.getCiudades()
    
  }

  getCiudades() {
    this.ciudadesService.obtnerCiudades().subscribe(resp => {
     
     if (resp.data.length > 0) {
     
       this.ciudades = resp.data
       this.ocultarTabla = true
     }else{
      this.ocultarTabla = false
     }
    })
  }

  eliminarCiudad(id:number){
    Swal.fire({
      title: 'Estas seguro?',
      text: 'No podrás revertir esto.!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.ciudadesService.eliminarCiudad(id)
        .subscribe( resp => {
          this.getCiudades()
          Swal.fire('Deleted!', 'Ciudad Eliminada.', 'success');
        })
       
      }
    });
  }


}
