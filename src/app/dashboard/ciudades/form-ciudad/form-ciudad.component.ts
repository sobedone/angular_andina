import { Component, OnInit } from '@angular/core';
import { CiudadesService } from '../../services/ciudades.service';
import { Router } from '@angular/router';
import * as L from 'leaflet';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-form-ciudad',
  templateUrl: './form-ciudad.component.html',
  styleUrls: ['./form-ciudad.component.css'],
})
export class FormCiudadComponent implements OnInit {
  ciudadNombre: string = "";
  latitud: string = "";
  longitud: string = "";

  constructor(private ciudadServices: CiudadesService, private route: Router) {}

  ngOnInit(): void {
    this.mostrarMapa();
  }

  reiniciarSeleccion() {
    this.ciudadNombre=""
    this.latitud =""
    this.longitud=""

    window.location.reload()
    // this.route.navigate(['/dashboard/ciudades/crear']);
  }

  mostrarMapa() {
    let arrayMarkadores = [];

    let map = L.map('map').setView([3.44763, -76.53162], 10);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors>',
      maxZoom: 18,
    }).addTo(map);

    map.on('click', (e:any) =>  {
      let latitud = e.latlng
      
      this.ciudadServices.obtenerNombreCiudad(latitud.lat,latitud.lng)
      .subscribe( resp => {
        if (resp.address.county) {
          this.ciudadNombre = resp.address.county
          this.latitud = latitud.lat
          this.longitud = latitud.lng
          L.marker([latitud.lat, latitud.lng]).addTo(map);
        }
      })
    });
  }

  guardarCiudad(){

    if (this.ciudadNombre == '') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: `No seleccionaste una dirección`,
      })
      return 
    }
    const formData = new FormData();
    formData.append('nombre', this.ciudadNombre);
    formData.append('lat', this.latitud);
    formData.append('lng', this.longitud);

    this.ciudadServices.guardarCiudades(formData)
    .subscribe(resp => {
      console.log(resp)
      Swal.fire({
        icon: 'success',
        title: 'Good job!',
        text: `Ciudad creada satisfactoriamente`,
      })
    })
    

  }
}
