import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './productos/productos.component';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from '../guards/auth.guard';
import { ViewComponent } from './productos/view/view.component';
import { FormComponent } from './productos/form/form.component';
import { FormCiudadComponent } from './ciudades/form-ciudad/form-ciudad.component';

const routes: Routes = [
  {
    path:'',
    component:DashboardComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path:'productos',
        component:ProductosComponent
      },
      {
        path:'productos/crear',
        component:FormComponent
      },
      {
        path:'productos/editar/:productoId',
        component:FormComponent
      },
      {
        path:'productos/ver/:productoId',
        component:ViewComponent
      },
      
      {
        path:'ciudades',
        component:CiudadesComponent
      },
      {
        path:'ciudades/crear',
        component:FormCiudadComponent
      },
      {
        path:'**',
        redirectTo:'/dashboard/productos'
      }
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
