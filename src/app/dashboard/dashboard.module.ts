import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ProductosComponent } from './productos/productos.component';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { ViewComponent } from './productos/view/view.component';
import { FormComponent } from './productos/form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormCiudadComponent } from './ciudades/form-ciudad/form-ciudad.component';


@NgModule({
  declarations: [
    ProductosComponent,
    CiudadesComponent,
    DashboardComponent,
    ViewComponent,
    FormComponent,
    FormCiudadComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
