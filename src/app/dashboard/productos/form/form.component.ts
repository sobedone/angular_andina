import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductosService } from '../../services/productos.service';
import { CiudadesService } from '../../services/ciudades.service';
import { switchMap,tap } from 'rxjs/operators';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  productosForm!: FormGroup;
  ciudades: any[] = [];
  ciudadId: any[] = [];
  selectedFile!: File;
  actualiza:boolean = false
  formType:string='crear'
  productoId!:number;
  constructor(
    private formBuilder: FormBuilder,
    private productoService: ProductosService,
    private router: Router,
    private ciudadService: CiudadesService,
    private activeRoute:ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form();
    this.obtenerCiudad();
    this.activeRoute.params
    .pipe(
      switchMap( ({productoId}) => {
      
        if(productoId){
          this.actualiza = true
          this.formType ='Editar'
          this.productoId = productoId
        }else{
         
          this.actualiza = false
          this.formType ='Crear'
        }
        return this.productoService.obtenerProducto(productoId)
      }),
      tap()
      )
    .subscribe(resp => {
     
      
      
        const {cantidad,nombre,observaciones,precio,ciudades_id} = resp.data
        if (ciudades_id) {
          this.ciudadId = ciudades_id
          this.productosForm.patchValue({
            nombre: nombre,
            precio: precio,
            cantidad: cantidad,
            observaciones: observaciones,
            ciudad_id: JSON.stringify(this.ciudadId),
          })
          
        }
      
     
    
      
    })

  }

  get getCiudades() {
    return this.ciudades;
  }
  form() {
    this.productosForm = this.formBuilder.group({
      nombre: [null, [Validators.required]],
      precio: [null, [Validators.required]],
      cantidad: [null, [Validators.required]],
      imagen: [null],
      observaciones: [null, [Validators.required]],
      ciudad_id: [null],
    });
  }

  guardarEditar() {
    
    if (this.productosForm.invalid){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        html: `${ this.mensajeValidacion(this.productosForm.value,this.actualiza)}`,
      })

     
     
      return;
    } 
   
    if (this.actualiza) {
    
      this.productoService.actualizarproducto(this.productosForm.value,this.productoId)
      .subscribe(resp => {
       
        Swal.fire({
          icon: 'success',
          title: 'Good job!',
          text: `Producto actualizado!`,
        })
        this.router.navigate(['/dashboard/productos'])
      })
    }else{
      
      
      this.productoService
        .guardarProductos(this.productosForm.value)
        .subscribe((resp) => {
         
          Swal.fire({
            icon: 'success',
            title: 'Good job!',
            text: `Producto creado!`,
          })

        }, (error) => {
          console.log(error);
        });
    }
  }

  obtenerCiudad() {
    this.ciudadService.obtnerCiudades().subscribe((resp) => {
      this.ciudades = resp.data;
    });
  }

  ciudadesId(event: any) {
    
    if (event.target.checked) {
    
      if (this.ciudadId) {
        let existe = this.ciudadId.find((e) => e == event.target.value);
  
        if (!existe) {
          
          this.ciudadId.push(parseInt(event.target.value));
        }
        
      }
    } else {
      if (this.ciudadId) {
        let existe = this.ciudadId.findIndex((e) => e == event.target.value);
        this.ciudadId.splice(existe, 1);
        
      }
    }

    this.productosForm.patchValue({
      ciudad_id: JSON.stringify(this.ciudadId),
    });
  }

  obtenerImagen(event: any) {
    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0];
      this.productosForm.patchValue({
        imagen: this.selectedFile,
      });
    }
    // let file = event.target.files[0]

    // this.productosForm.get('imagen')?.updateValueAndValidity()
  }

  checkedCiudades(ciudad:any){
   
    
    if (this.ciudadId) {
     
      if (this.ciudadId.length>0) {
       
        let existe = this.ciudadId.find(e=> e == ciudad.id)
        if (existe) {
          return true
        }else{
          return false
        }
        
      }else{
        return false
      }
    }else{
      return false
    }
  }

  mensajeValidacion (data:any,tipo:boolean){
    
    let li=''
    if (!tipo) {
      if (data.nombre == null) {
        li+= `<li>El nombre del producto es requerido</li>`
      }
      if (data.precio == null) {
        li+= `<li>El precio es requerido</li>`
      }
      if (data.cantidad == null) {
        li+= `<li>La cantidad es requerida</li>`
      }
      if (data.observaciones == null ) {
        li+= `<li>La observación es requerida</li>`
      }
      if (data.imagen == null) {
        li+= `<li>La imagen es requerida </li>`
      }
      
      
     
    }else{
      if (data.nombre == null) {
        li+= `<li>El nombre del producto es requerido</li>`
      }
      if (data.precio == null) {
        li+= `<li>El precio es requerido</li>`
      }
      if (data.cantidad == null) {
        li+= `<li>La cantidad es requerida</li>`
      }
      
      if (data.observaciones == null ) {
        li+= `<li>La observación es requerida</li>`
      }
      
    }

    let message = `
      <ol>
        ${li}
      </ol>
    `

    return message
  }
}
