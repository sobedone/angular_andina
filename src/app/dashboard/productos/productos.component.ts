import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../services/productos.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css'],
})
export class ProductosComponent implements OnInit {
  constructor(private productoService: ProductosService) {}
  productos: any[] = [];
  ocultarTabla: boolean = false;
  rutaBtn: string = '/dashboard/productos/crear';
  ngOnInit(): void {
    this.getProductos();
  }

  getProductos() {
    this.productoService.obtenerProductos().subscribe((resp) => {
      if (resp.data.length > 0) {
        this.productos = resp.data;
        this.ocultarTabla = true;
      }
    });
  }

  eliminarProducto(id: number) {
    Swal.fire({
      title: 'Estas seguro?',
      text: 'No podrás revertir esto.!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.productoService.eliminarProducto(id).subscribe((resp) => {
          // console.log(resp)
          this.getProductos();
          Swal.fire('Deleted!', 'Producto Eliminado.', 'success');
        });
      }
    });
  }
}
