import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductosService } from '../../services/productos.service';
import { switchMap,tap } from 'rxjs/operators';
import * as L from 'leaflet';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  producto:any = {};
  constructor(private activeRoute:ActivatedRoute,private productoService:ProductosService) { }

  ngOnInit(): void {
    this.obtenerProducto()
    
  }

  obtenerProducto(){
    this.activeRoute.params
    .pipe(
      switchMap( ({productoId}) => this.productoService.obtenerProducto(productoId)),
      tap()
      )
    .subscribe(resp => {
      this.producto = resp.data
      this.mostrarMapa()
    })
  }

  mostrarMapa (){
    
    let arrayMarkadores =[]
   
    let map= L.map('map').setView([3.44763, -76.53162], 3);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors>',
      maxZoom: 18,
    }).addTo(map);
    if (this.producto.latitudes.length > 0) {
        let latitudes = this.producto.latitudes
        latitudes.forEach((element:any) => {
         
          L.marker([element.lat, element.long]).addTo(map);
        });
    }
   
    
  }

  obtenerLatitud(e:any){
    console.log(e)
  }
}
