import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class CiudadesService {
  uri_api:string = 'http://127.0.0.1:8000/api'
  token:string =""
  constructor(private http:HttpClient) {
    this.token = localStorage.getItem('token') || ''
  }


  obtnerCiudades():Observable<any>{
    this.token = localStorage.getItem('token') || ''
    const headers= {
      'Authorization': 'Bearer '+this.token
    }
    const url =`${this.uri_api}/ciudades`
    return this.http.get<any>(url,{headers})
  }
  eliminarCiudad(id:number):Observable<any>{
    const headers= {
      'Authorization': 'Bearer '+this.token
    }
    const url =`${this.uri_api}/ciudades/${id}`
    return this.http.delete<any>(url,{headers})
  }

  obtenerNombreCiudad(lat:number,long:number):Observable<any>{
    
    const url =`https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${long}`
    return this.http.get<any>(url)
  }

  guardarCiudades(data:any):Observable<any>{
    const headers= {
      'Authorization': 'Bearer '+this.token
    }
    const url =`${this.uri_api}/ciudades`
    return this.http.post<any>(url,data,{headers})
  }
}
