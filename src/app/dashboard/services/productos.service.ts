import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  uri_api:string = 'http://127.0.0.1:8000/api'
  token:string =""
  
  constructor(private http:HttpClient) {
    this.token = localStorage.getItem('token') || ''
  }



  obtenerProductos():Observable<any>{
    this.token = localStorage.getItem('token') || ''
    const headers= {
      'Authorization': 'Bearer '+this.token
    }
    const url =`${this.uri_api}/productos`
    return this.http.get<any>(url,{headers})
  }

  eliminarProducto(id:number):Observable<any>{
    const headers= {
      'Authorization': 'Bearer '+this.token
    }
    const url =`${this.uri_api}/productos/${id}`
    return this.http.delete<any>(url,{headers})
  }

  obtenerProducto(id:number):Observable<any>{
    const headers= {
      'Authorization': 'Bearer '+this.token
    }
    const url =`${this.uri_api}/productos/${id}`
    return this.http.get<any>(url,{headers})
  }

  guardarProductos(data:any):Observable<any>{
    
    const headers = new HttpHeaders({
      'Authorization':'Bearer '+this.token
    })
    const formData = new FormData();
    formData.append('nombre', data.nombre);
    formData.append('precio', data.precio);
    formData.append('cantidad', data.cantidad);
    formData.append("imagen", data.imagen, data.imagen.name);
    formData.append('observaciones', data.observaciones);
    formData.append('ciudad_id', data.ciudad_id);

    const url =`${this.uri_api}/productos` 
    return this.http.post<any>(url,formData,{headers})
  }

  actualizarproducto(data:any,id:number):Observable<any>{
    
    const headers = new HttpHeaders({
      'Authorization':'Bearer '+this.token
    })
    const formData = new FormData();
   
    formData.append('nombre', data.nombre);
    formData.append('precio', data.precio);
    formData.append('cantidad', data.cantidad);
    if (data.imagen) {
      formData.append("imagen", data.imagen, data.imagen.name);
    }else{
      formData.append("imagen",'');

    }
    formData.append('observaciones', data.observaciones);
    formData.append('ciudad_id', data.ciudad_id);
    formData.append('_method','PUT')
    const url =`${this.uri_api}/productos/${id}` 
    return this.http.post<any>(url,formData,{headers})
  }

}
