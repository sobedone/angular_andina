import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  @Input() nombre:string =''
  @Input() ruta:string=''
  ngOnInit(): void {
    console.log(this.ruta)
  }

}
